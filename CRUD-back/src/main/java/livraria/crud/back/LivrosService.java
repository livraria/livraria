/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package livraria.crud.back;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("/livros")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LivrosService {

    static Integer contador = 1;
    List<Livro> livros;

    public LivrosService() {
        livros = new ArrayList<>();
        Livro l1 = new Livro();
        l1.setDescricao("Senhor dos aneis");
        l1.setId(0);
        l1.setPreco(50.00);
        livros.add(l1);
    }

    @GET
    public List<Livro> getLivros() {
        return livros;

    }

    @POST
    public Livro adicionar(Livro livro) {
        livro.setId(contador++);
        livros.add(livro);
        return livro;
    }

    @PUT
    @Path("{id}")
    public Livro atualizar(@PathParam("id") Integer id, Livro livro) {
        for (Livro l : livros) {
            if (l.getId().equals(id)) {
                l.setDescricao(livro.getDescricao());
                l.setPreco(livro.getPreco());
                return l;
            }
        }
        return null;
    }

    @DELETE
    @Path("{id}")
    public Livro excluir(@PathParam("id") Integer id) {
        for (Livro l : livros) {
            if (l.getId().equals(id)) {
                livros.remove(l);
                return l;
            }
        }
        return null;
    }

    @GET
    @Path("{id}")
    public Livro getLivro(@PathParam("id") Integer id, Livro livro) {

        for (Livro l : livros) {
            if (l.getId().equals(id)) {
                return l;
            }
        }
        return null;
    }

}
