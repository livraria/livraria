
package livraria.crud.back;

public class Livro {
    private Integer id;
    private String descricao;
    private Double preco;

    public Livro() {
    }

    public Livro(Integer id, String descricao, Double preco) {
        
        
        this.id = id;
        this.descricao = descricao;
        this.preco = preco;
    }

    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
    
    
}
