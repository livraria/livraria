var app = angular.module('livrosApp',[]);
           app.controller('livrosController', function($scope,livrosService) {
             
              $scope.livro = {};
              listar();
              
             function listar(){
               livrosService.listar().then(function(resposta){
               $scope.livros = resposta.data;                
          });
      }; 
              $scope.salvar = function(livro){             
               livrosService.salvar(livro).then(listar);                
                $scope.livro = {};
            };
            
            $scope.excluir = function(livro){
              livrosService.excluir(livro).then(listar);
            };
            
            $scope.cancelar = function(){
                $scope.livro = {};
            };
            
            $scope.editar = function(livro) {
                $scope.livro = angular.copy(livro);
            };
            
    });
           
           app.service('livrosService', function($http){
               var api = 'http://localhost:8080/api/webresources/livros';
               
            this.listar = function(){
                return $http.get(api);
            };
            
            this.salvar = function(livro){                  
                 if(livro.id){
                     //put
                     return $http.put(api + '/'+ livro.id, livro);                    
                  }
              else{
                  //post
                  return $http.post(api, livro);
              }                
            };
            
            this.excluir = function(livro){
               return $http.delete(api + '/' + livro.id);
              
            };
            
           });

